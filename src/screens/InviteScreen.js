import React from 'react';
import InviteContainer from "../components/Invite/InviteContainer";


class InviteScreen extends React.Component {
  render() {
    return (
      this.props.match.params.key ?
        <InviteContainer {...this.props}/>: null
    )
  }
}


export default InviteScreen;