import React from 'react';
import UserListContainer from "../components/User/List/UserListContainer";


class UserListScreen extends React.Component {
  render() {
    return (
      <UserListContainer {...this.props} />
    )
  }
}


export default UserListScreen;