import React from 'react';
import UserDetailContainer from "../components/User/UserDetailContainer";
import UserCreateContainer from "../components/User/UserCreateContainer";


class UserDetailScreen extends React.Component {
  render() {
    return (
      this.props.match.params.id ?
        <UserDetailContainer {...this.props} /> :
        <UserCreateContainer {...this.props} />
    )
  }
}


export default UserDetailScreen;