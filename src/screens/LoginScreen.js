import React from 'react';
import LoginView from "../components/Login/LoginView";


class LoginScreen extends React.Component {
  render() {
    return (
      <LoginView {...this.props} />
    )
  }
}


export default LoginScreen;