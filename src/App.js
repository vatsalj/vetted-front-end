import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import './App.css';
import HeaderContainer from "./components/Header/HeaderContainer";
import Routes from "./Routes";
import Auth from './helpers/Auth';
import {ProgressBar} from "react-bootstrap";
import OrganizationAPI from "./helpers/OrganizationAPI";
import InvalidHostView from "./components/Miscellaneous/InvalidHostView";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false,
      isAuthenticating: true,
      token: "", // TODO Token shouldn't be part of this state as it can change during subsequent requests
      user: {},
      loading: false,
      invalidHost: false
    }
  }

  /**
   * Update user authentication status across the app & reset the state variables
   * @param authenticated Boolean value providing authentication status
   * @param userDetails Object containing user details
   */
  updateUserAuthState = ({authenticated, userDetails = {user: {}, token: ""}}) => {
    if (authenticated) {
      Auth.logIn({userDetails});
    } else {
      Auth.logOut();
    }
    this.setState({
      ...Auth.getUserDetails(),
      isAuthenticated: authenticated,
    });
  };

  async componentDidMount() {
    this.setState({
      loading: true,
    });
    try {
      await OrganizationAPI.validateHost();
    } catch (error) {
      // Hostname is invalid, show the default screen
      this.setState({
        invalidHost: true,
        loading: false
      });
      return;
    }

    const userDetails = Auth.getUserDetails();
    this.setState({
      token: userDetails.token,
      user: userDetails.user,
      isAuthenticated: userDetails.authenticated,
      isAuthenticating: false,
      loading: false
    });
  }

  showLoader = () => {
    this.setState({
      loading: true,
    });
  };

  hideLoader = () => {
    this.setState({
      loading: false,
    });
  };

  render() {
    if (!this.state.loading && this.state.invalidHost) {
      return <InvalidHostView />
    }

    const routeProps = {
      isAuthenticated: this.state.isAuthenticated,
      isAdmin: this.state.user.isAdmin,
      user: this.state.user,
      showLoader: this.showLoader,
      hideLoader: this.hideLoader,
      updateUserAuthState: this.updateUserAuthState
    };

    return (
      !this.state.isAuthenticating &&
      <div className="App">
        <HeaderContainer {...routeProps} />
        {
          this.state.loading &&
          <ProgressBar className="App-progress">
            <ProgressBar active bsStyle="warning" now={100} key={3} />
          </ProgressBar>
        }
        <div className="container">
          <Routes routeProps={routeProps} />
        </div>
      </div>
    );
  }
}

export default withRouter(App);