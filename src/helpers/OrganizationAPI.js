import axios from 'axios';
import Config from '../Config';


const OrganizationAPI = {
  validateHost: async () => {
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('validHost')) {
        return resolve(true);
      }
      axios.get(`${Config.baseUri}/api/v1/organization/validate-host/`)
        .then(response => {
        localStorage.setItem('validHost', "yes");
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  }
};


export default OrganizationAPI;
