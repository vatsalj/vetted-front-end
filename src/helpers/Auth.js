const Auth = {
  _validateSessionData(data) {
    // TODO add validation logic to data
    return true;
  },

  saveUserSession(data) {
      localStorage.setItem('userDetails', JSON.stringify(data));
  },

  validateAuthToken(token) {
    return !!token;
  },

  getToken() {
    const userDetails = this.getUserDetails();
    return userDetails.token;
  },

  /**
   * Provide consistent user session details
   * @returns {{user: (userDetails.user|{}), token: string}}
   */
  getUserDetails() {
    const userDetailsStr = localStorage.getItem('userDetails');
    let userDetails = {
      token: "",
      user: {}
    }, authenticated = false;

    if (userDetailsStr) {
      // temporary variable to store user details before checking auth status
      const _userDetails = JSON.parse(userDetailsStr);
      if (this.validateAuthToken(_userDetails.token)) {
        authenticated = true;
        userDetails = _userDetails;
      }
    }
    let user = userDetails.user;
    user['isAdmin'] = user.hasOwnProperty('org_user_profile') && user.org_user_profile.role === 0;
    return {
      token: userDetails.token,
      user: user,
      authenticated: authenticated,
    }
  },

  logIn({userDetails}) {
    if (this._validateSessionData(userDetails)) {
      this.saveUserSession(userDetails);
    } else {
      throw new Error("Invalid data provided for session");
    }
  },

  logOut() {
    this.saveUserSession({
      token: "",
      user: {}
    });
  },
};


export default Auth;