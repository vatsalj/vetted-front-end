import React from "react";
import {Route} from "react-router-dom";

/**
 * Pass component props using render method rather than using inline function to component
 * as it creates a new component rather than updating the existing one
 * @param C Screen component that is to receive props
 * @param cProps Props to be sent to the component
 * @param rest Rest of the props
 * @returns {*}
 */
export default ({ component: C, props: cProps, ...rest }) =>
  <Route {...rest} render={props => <C {...props} {...cProps} />} />;
