import React from 'react';

import LoginAPI from './LoginAPI';
import {Alert, Button, ControlLabel, FormControl, FormGroup} from 'react-bootstrap';
import './Login.css';
import {withRouter} from "react-router-dom";

class LoginView extends React.Component {
  constructor(props) {
    debugger;
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: {
        email: '',
        password: ''
      },
      apiErrors: []
    };
  }

  componentDidMount() {
    debugger;
    if (this.props.isAuthenticated) {
      this._redirectToProfile({id: this.props.user.id});
    }
  }

  validateForm = () => {
    return this.state.email.length > 0 && this.state.password.length > 0
  };

  checkValidationState = ({field}) => {
    // TODO
    if (this.state.errors[field].length === 0) {
      return null;
    } else {
      return 'error';
    }
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  _redirectToProfile = ({id}) => {
    this.props.history.replace(`/users/${id}`);
  };

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({
      apiErrors: []
    });
    this.props.showLoader();
    let response;
    try {
      response = await LoginAPI.loginUser({
        email: this.state.email,
        password: this.state.password,
      });
      this.props.updateUserAuthState({authenticated: true, userDetails: response.data});
      this._redirectToProfile({id: response.data.id});
    } catch (error) {
      this.setState({
        apiErrors: error.data.non_field_errors
      })
    }
    this.props.hideLoader();
  };

  render() {
    return (
      <div className="Login">
        {
          this.state.apiErrors.map(error => {
            return (
              <Alert bsStyle="warning">
                {error}
              </Alert>
            )
          })
        }
        <form onSubmit={this.handleSubmit} noValidate>
          <FormGroup
            controlId="email"
            bsSize="large"
            validationState={this.checkValidationState({field: 'email'})}>
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup
            controlId="password"
            bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            bsStyle="primary"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
        </form>
      </div>
    )
  }
}

export default withRouter(LoginView);
