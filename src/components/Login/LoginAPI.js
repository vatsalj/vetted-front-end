import axios from 'axios';
import Config from '../../Config';


class LoginAPI {
  static loginUser = ({email, password}) => {
    return new Promise((resolve, reject) => {
      axios.post(`${Config.baseUri}/api/v1/organization/login/`, {
        email: email,
        password: password,
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  }
}


export default LoginAPI;
