import {Alert} from "react-bootstrap";
import React from 'react';

const InvalidHostView = () => {
  return (
    <div>
      <Alert bsStyle="danger">
        <h4>Oh snap! You seem to be lost padawan!</h4>
        <p>
          This doesn't look like a valid organization to us, you might wanna check the URL again.
        </p>
      </Alert>
    </div>
  )
};


export default InvalidHostView;
