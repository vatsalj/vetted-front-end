import React from 'react';
import InviteAPI from "./InviteAPI";
import InviteView from "./InviteView";
import {Alert} from "react-bootstrap";


class InviteContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      loading: false,
      key_valid: true
    }
  }

  async componentDidMount() {
    this.setState({
      loading: true,
    });
    this.props.showLoader();
    try {
      await InviteAPI.validateInviteKey({key: this.props.match.params.key});
    } catch (error) {
      this.setState({
        errors: error.data,
        key_valid: false
      });
    }
    this.props.hideLoader();
    this.setState({
      loading: false,
    });
  }

  onInviteAccepted = async ({password, confirm_password}) => {
    try {
      await InviteAPI.acceptInvite({
        key: this.props.match.params.key,
        password: password,
        confirm_password: confirm_password
      });
      this.props.history.push('/');
    } catch(error) {
      this.setState({
        errors: error.data
      });
    }
  };

  onInviteRejected = async () => {
    try {
      await InviteAPI.rejectInvite({key: this.props.match.params.key});
    } catch (error) {
      this.setState({
        errors: error.data
      });
    }
  };

  render() {
    if (this.state.loading) {
      return null;
    }
    const flattened_errors = Object.entries(this.state.errors).map(([key, value]) => {
      return `${key}: ${value}`
    });
    // TODO: Container is managing view for errors, should be fixed
    return (
      <div>
        {
          flattened_errors.map((error, index) => {
            return (
              <Alert bsStyle="danger" key={index}>
                {error}
              </Alert>
            )
          })
        }
        {
          this.state.key_valid &&
            <InviteView
              onInviteAccepted={this.onInviteAccepted}
              onInviteRejected={this.onInviteRejected}
            />
        }
      </div>
    )
  }
}

export default InviteContainer;
