import axios from 'axios';
import Config from '../../Config';


const InviteAPI = {
  validateInviteKey: ({
    key
  }) => {
    return new Promise((resolve, reject) => {
      axios.post(`${Config.baseUri}/api/v1/invite/validate-key/`, {
        key: key
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  },

  acceptInvite: ({
    key,
    password,
    confirm_password
  }) => {
    return new Promise((resolve, reject) => {
      axios.post(`${Config.baseUri}/api/v1/invite/accept/`, {
        key: key,
        password: password,
        confirm_password: confirm_password
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  },

  rejectInvite: ({key}) => {
    return new Promise((resolve, reject) => {
      axios.post(`${Config.baseUri}/api/v1/invite/reject/`, {
        key: key
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  }
};


export default InviteAPI;
