import React from 'react';
import {Alert, Button, ControlLabel, FormControl, FormGroup, Modal} from "react-bootstrap";
import PropType from 'prop-types';


class InviteView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAcceptanceModal: false,
      showRejectionModal: false,
      password: "",
      confirm_password: "",
      error: ""
    }
  }

  showAcceptanceModal = () => {
    this.setState({showAcceptanceModal: true});
  };

  showRejectionModal = () => {
    this.setState({showRejectionModal: true});
  };

  hideAcceptanceModal = () => {
    this.setState({showAcceptanceModal: false});
  };

  hideRejectionModal = () => {
    this.setState({showAcceptanceModal: false});
  };

  onInputChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleAcceptance = async (event) => {
    event.preventDefault();
    if (this.state.password !== this.state.confirm_password) {
      this.setState({error: "Passwords do not match, please try again."});
      return;
    } else {
      this.setState({error: ""});
    }
    await this.props.onInviteAccepted({password: this.state.password, confirm_password: this.state.password});
  };

  handleRejection = async () => {
    await this.props.onInviteRejected();
  };

  render = () => {
    return (
      <div>
        <div className="well" style={wellStyles}>
          <Button bsStyle="success" bsSize="large" onClick={this.showAcceptanceModal} block>
            Accept
          </Button>
          <Button bsStyle="warning" bsSize="large" onClick={this.showRejectionModal} block>
            Reject
          </Button>
        </div>
        <Modal
          show={this.state.showAcceptanceModal}
          onHide={this.hideAcceptanceModal}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              Please enter a password for your account below
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {
              this.state.error !== "" &&
              <Alert bsStyle="warning">{this.state.error}</Alert>
            }
            <form onSubmit={this.handleAcceptance} noValidate>
              <FormGroup
                controlId="password"
                bsSize="large">
                <ControlLabel>Password</ControlLabel>
                <FormControl
                  value={this.state.password}
                  type="password"
                  onChange={this.onInputChange}
                />
              </FormGroup>
              <FormGroup
                controlId="confirm_password"
                bsSize="large">
                <ControlLabel>Confirm password</ControlLabel>
                <FormControl
                  value={this.state.confirm_password}
                  type="password"
                  onChange={this.onInputChange}
                />
              </FormGroup>
              <Button
                block
                bsSize="large"
                bsStyle="primary"
                // disabled={!this.validateForm()}
                type="submit"
              >
                Save
              </Button>
            </form>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.hideAcceptanceModal}>Close</Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.showRejectionModal}
          onHide={this.hideRejectionModal}
          container={this}
          aria-labelledby="contained-modal-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">
              Reject Invite
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Button bsStyle="danger" onClick={this.handleRejection}>Confirm</Button>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.hideRejectionModal}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}

const wellStyles = { maxWidth: 400, margin: '0 auto 10px' };

InviteView.propTypes = {
  onInviteAccepted: PropType.func.isRequired,
  onInviteRejected: PropType.func.isRequired
};


export default InviteView;
