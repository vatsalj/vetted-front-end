import React from 'react';
import HeaderView from './HeaderView';


class HeaderContainer extends React.Component {
  handleLogout = () => {
    this.props.updateUserAuthState({
      authenticated: false
    });
  };

  render() {
    return (
      <HeaderView
        handleLogout={this.handleLogout}
        isAuthenticated={this.props.isAuthenticated}
        isAdmin={this.props.isAdmin}
      />
    )
  }
}


export default HeaderContainer;