import React from 'react';
import {MenuItem, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import PropTypes from 'prop-types';


const HeaderView = ({isAdmin, isAuthenticated, handleLogout}) => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <a href="/">Organization</a>
        </Navbar.Brand>
      </Navbar.Header>
      {
        isAdmin &&
          <Nav>
            <NavDropdown eventKey={3} title="Admin" id="basic-nav-dropdown">
              <MenuItem href='/users'>
                View users
              </MenuItem>
              <MenuItem href='/users/create'>
                Invite new user
              </MenuItem>
            </NavDropdown>
          </Nav>
      }
      {
        isAuthenticated &&
        <Nav>
          <NavItem onClick={handleLogout} href="/">
            Logout
          </NavItem>
        </Nav>
      }
    </Navbar>
  )
};

HeaderView.propTypes = {
  isAdmin: PropTypes.bool.isRequired,
  handleLogout: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
};

export default HeaderView;
