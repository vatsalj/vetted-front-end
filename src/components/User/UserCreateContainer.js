import React from 'react';
import UserAPI from './UserAPI';
import UserFormView from './UserFormView';


class UserCreateContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      error: ""
    }
  }

  onSubmit = async ({data}) => {
      return UserAPI.createUser(data);
  };

  render() {
    return (
      <UserFormView
        handleSubmit={this.onSubmit}
        errors={this.state.errors}
        history={this.props.history}
      />
    )
  }
}

export default UserCreateContainer;
