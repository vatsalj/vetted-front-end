import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";


class UserFormView extends React.Component {
  static roleChoices = {
    0: 'Admin',
    1: 'Standard'
  };

  static defaultProps = {
    user: {
      first_name: "",
      last_name: "",
      role: Object.keys(UserFormView.roleChoices)[0],
      email: "",
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      ...props.user,
      errors: {}
    }
  }

  onInputChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await this.props.handleSubmit({data: this.state});
      this.props.history.push('/users');
    } catch(error) {
      console.error(error);
      if (error.data) {
        this.setState({
          errors: error.data,
        });
      }
    }
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} noValidate>
        <FormGroup
          controlId="first_name"
          bsSize="large">
          <ControlLabel>First Name</ControlLabel>
          <FormControl
            value={this.state.first_name}
            onChange={this.onInputChange}
          />
        </FormGroup>
        <FormGroup
          controlId="last_name"
          bsSize="large">
          <ControlLabel>Last Name</ControlLabel>
          <FormControl
            value={this.state.last_name}
            onChange={this.onInputChange}
          />
        </FormGroup>
        <FormGroup
          controlId="email"
          validationState={this.state.errors.email ? "error": null}
          bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            value={this.state.email}
            onChange={this.onInputChange}
          />
          {
            this.state.errors.email &&
              <HelpBlock>{this.state.errors.email}</HelpBlock>
          }
        </FormGroup>
        <FormGroup
          controlId="role"
          bsSize="large">
          <ControlLabel>Role</ControlLabel>
          <FormControl
            autoFocus
            componentClass="select"
            value={this.state.role}
            onChange={this.onInputChange}
          >
            {
              Object.entries(UserFormView.roleChoices).map(([key, value]) => {
                return (
                  <option key={key} value={key}>{value}</option>
                )
              })
            }
          </FormControl>
          {
            this.state.errors.role &&
            <HelpBlock>{this.state.errors.role}</HelpBlock>
          }
        </FormGroup>
        <Button
          block
          bsSize="large"
          bsStyle="primary"
          // disabled={!this.validateForm()}
          type="submit"
        >
          Save
        </Button>
      </form>
    );
  }
}


export default UserFormView;