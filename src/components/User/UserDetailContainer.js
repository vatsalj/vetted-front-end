import React from 'react';
import UserAPI from './UserAPI';
import UserFormView from './UserFormView';


class UserDetailContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      first_name: "",
      last_name: "",
      email: "",
      role: 1,
      loading: false,
      error: ""
    }
  }

  async componentDidMount() {
    this.setState({
      loading: true,
    });
    try {
      const response = await UserAPI.getUser({id: this.props.match.params.id});
      this.setState({
        id: response.data.id,
        first_name: response.data.first_name,
        last_name: response.data.last_name,
        email: response.data.email,
        role: response.data.org_user_profile.role
      });
      this.setState({
        loading: false,
      });
    } catch(error) {
      this.setState({
        loading: false,
      });
      if (error.status === 404) {
        this.props.history.replace('/users')
      }
    }
  }

  onDetailSubmit = async ({data}) => {
    return UserAPI.editUser({id: this.state.id, ...data});
  };

  render() {
    let {error, loading, id, ...user} = this.state;
    return (
      loading ? null:
        <UserFormView
          user={user}
          handleSubmit={this.onDetailSubmit}
          history={this.props.history}
        />
    )
  }
}

export default UserDetailContainer;
