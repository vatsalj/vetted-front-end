import axios from 'axios';
import Config from '../../Config';
import Auth from '../../helpers/Auth';


const UserAPI = {
  getUsers: function() {
    const token = Auth.getToken();
    return new Promise((resolve, reject) => {
      axios.get(`${Config.baseUri}/api/v1/user/`, {
        headers: {
          'Authorization': `Token ${token}`
        }
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  },

  getUser: function({id}) {
    const token = Auth.getToken();
    return new Promise((resolve, reject) => {
      axios.get(`${Config.baseUri}/api/v1/user/${id}/`, {
        headers: {
          'Authorization': `Token ${token}`
        }
      }).then(response => {
        return resolve(response);
      }).catch(function (error) {
        return reject(error.response);
      });
    });
  },

  createUser: function({
    first_name,
    last_name,
    email,
    role
  }) {
    const token = Auth.getToken();
    return new Promise((resolve, reject) => {
      axios.post(`${Config.baseUri}/api/v1/user/`, {
        first_name: first_name,
        last_name: last_name,
        email: email,
        org_user_profile: {
          role: role
        }
      }, {
        headers: {
          'Authorization': `Token ${token}`
        }
      }).then(response => {
        debugger;
        return resolve(response);
      }).catch(function (error) {
        debugger;
        return reject(error.response);
      });
    });
  },

  editUser: function({
    id,
    first_name,
    last_name,
    email,
    role
  }) {
    const token = Auth.getToken();
    return new Promise((resolve, reject) => {
      axios.patch(`${Config.baseUri}/api/v1/user/${id}/`, {
        first_name: first_name,
        last_name: last_name,
        email: email,
        org_user_profile: {
          role: role
        }
      }, {
        headers: {
          'Authorization': `Token ${token}`
        }
      }).then(response => {
        debugger;
        return resolve(response);
      }).catch(function (error) {
        debugger;
        return reject(error.response);
      });
    });
  },

  deleteUser: function({
    id
  }) {
    const token = Auth.getToken();
    return new Promise((resolve, reject) => {
      axios.delete(`${Config.baseUri}/api/v1/user/${id}/`,
        {
        headers: {
          'Authorization': `Token ${token}`
        }
      }).then(response => {
          return resolve(response);
      }).catch(error => {
        return reject(error);
      });
    });
  }
};


export default UserAPI;
