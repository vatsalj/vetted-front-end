import React from 'react';
import {Button, Glyphicon, PageHeader, Table} from "react-bootstrap";
import {Link} from "react-router-dom";


class UserListView extends React.Component {
  render() {
    if (this.props.users.length === 0) {
      return null;
    }
    return (
      <div>
        <PageHeader>
          User list
        </PageHeader>
        <Table responsive>
          <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            {
              this.props.isAdmin &&
                <th>Actions</th>
            }
          </tr>
          </thead>
          <tbody>
          {
            this.props.users.map(user => {
              return (
                <tr key={user.id}>
                  <td>{`${user.first_name} ${user.last_name}`.trim() || '-'}</td>
                  <td>{user.email}</td>
                  <td>{user.org_user_profile.role_display}</td>
                  {
                    this.props.isAdmin &&
                    <td>
                      <Link to={`/users/${user.id}`} id={user.id}>
                        <Button bsSize="xsmall" bsStyle="link">
                          <Glyphicon glyph="pencil"/>
                        </Button>
                      </Link>
                      <Button bsSize="xsmall" bsStyle="link" onClick={(event) => {
                        this.props.onDeletePress({id: user.id})
                      }}>
                        <Glyphicon glyph="trash"/>
                      </Button>
                    </td>
                  }
                </tr>
              )
            })
          }
          </tbody>
        </Table>
      </div>
    )
  }
}


export default UserListView;