import React from 'react';
import UserAPI from '../UserAPI';
import UserListView from './UserListView';


class UserListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.listChild = null;

    this.state = {
      users: [],
    }
  }

  async componentDidMount() {
    this.props.showLoader();
    try {
      const response = await UserAPI.getUsers();
      this.setState({
        users: response.data,
      });
      this.props.hideLoader();
    } catch(error) {
      this.props.hideLoader();
    }
  }

  onDeletePress = ({id}) => {
    this.props.showLoader();
    UserAPI.deleteUser({id}).then(response => {
      let users = this.state.users;
      const deletedUser = users.find(user => user.id === id);
      const deletedUserIndex = users.indexOf(deletedUser);
      users.splice(deletedUserIndex, 1);
      this.setState({
        users: users,
      });
      this.props.hideLoader();
    }).catch(error => {
      console.error(error);
      this.props.hideLoader();
    });
  };

  render() {
    return (
      <UserListView
        users={this.state.users}
        onDeletePress={this.onDeletePress}
        isAdmin={this.props.isAdmin}
        ref={this.listChild}
      />
    )
  }
}

export default UserListContainer;
