import React from "react";
import {Switch} from "react-router-dom";
import AppliedRoute from "./helpers/AppliedRoute";
import LoginScreen from "./screens/LoginScreen";
import UserListScreen from "./screens/UserListScreen";
import UserDetailScreen from "./screens/UserDetailScreen";
import InviteScreen from "./screens/InviteScreen";

export default ({routeProps}) =>
  <Switch>
    <AppliedRoute path="/" exact component={LoginScreen} props={routeProps}/>
    <AppliedRoute path="/users" exact component={UserListScreen} props={routeProps}/>
    <AppliedRoute path="/users/create" exact component={UserDetailScreen} props={routeProps}/>
    <AppliedRoute path="/users/:id" exact component={UserDetailScreen} props={routeProps}/>
    <AppliedRoute path="/invite/view/:key" exact component={InviteScreen} props={routeProps}/>
  </Switch>;
